const tasks = require("./routes/tasks");
const connection = require("./db");
const cors = require("cors");
const express = require("express");
const app = express();

connection(); // se connecte à la BDD

app.use(express.json());
app.use(cors());

app.use("/api/tasks", tasks);

const port = 8080; // port par défaut sur MongoDB
app.listen(port, () => console.log(`La base de donnée écoute au port 8080...`));
