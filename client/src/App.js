import React from "react";
import Tasks from "./Tasks";
import { Paper, TextField } from "@material-ui/core";
import { Checkbox } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import "./App.css";

/*
    Fonction permettant de changer le nom de la tâche passée en paramètre.
    L'affichage du nom de la tâche se fait via un span et le changement de nom via un input texte.
    Cette fonction permet de masquer le span et d'afficher l'input texte pour permettre à l'utilisateur de changer le nom de la tâche.
    Elle affiche également le bouton jaune pour sauvegarder le changement.
*/
function changeName(task) { 
    let textField = document.getElementsByClassName("changeNameTask");
    let label = document.getElementsByClassName('nomTache');
    let button = document.getElementsByClassName('saveChange');

    let index = 0;

    while (task.task !== label[index].innerHTML) {
        index = index + 1;
    }
    label[index].style.display = 'none';
    textField[index].style.display = 'inline';
    button[index].style.display = 'inline';
}

/*
    Fonction appellée pour valider le changement de nom de la tâche.
    Elle va récupérer la valuer inscrit dans l'input texte et va le transmettre au span.
    Elle va afficher le span, masquer l'input texte et le bouton de sauvegarde.
    Elle va évidemment inscrire le nouveau nom de la tâche dans la BDD.
*/
function saveChange(task, objet) {
    let textField = document.getElementsByClassName("changeNameTask");
    let label = document.getElementsByClassName('nomTache');
    let button = document.getElementsByClassName('saveChange');

    let index = 0;

    while (task.task !== label[index].innerHTML) {
        index = index + 1;
    }

    objet.handleModif(task._id, textField[index].value);
    label[index].style.display = 'inline';
    textField[index].style.display = "none";
    button[index].style.display = 'none';
}


class App extends Tasks {
    state = { tasks: [], currentTask: "" };
    render() {
        const { tasks } = this.state;
        return (
            <div className="App flex">
                <Paper elevation={3} className="container">
                    <div className="heading">Gestionnaire de TO-DO List</div>
                    <form
                        onSubmit={this.handleSubmit}
                        className="flex"
                        style={{ margin: "15px 0" }}
                    >
                        <TextField
                            variant="outlined"
                            size="small"
                            style={{ width: "80%" }}
                            value={this.state.currentTask}
                            required={true}
                            onChange={this.handleChange}
                            placeholder="Ajouter une tâche"
                        />
                        <Button
                            style={{ height: "40px" }}
                            color="primary"
                            variant="outlined"
                            type="submit"
                            id="addButton"
                            class="btn btn-success"
                        >
                            Ajouter tâche
                        </Button>
                    </form>
                    <div id="taches">
                        Vos tâches :
                    </div>
                    <div>
                        {tasks.map((task) => ( //parcourt les tâches dans la BDD et les affiche
                            <Paper
                                key={task._id}
                                className="flex task_container"
                            >
                                <Checkbox
                                    checked={task.completed}
                                    onClick={() => this.handleUpdate(task._id)}
                                    color="primary"
                                />
                                <div
                                    className={
                                        task.completed
                                            ? "task line_through"
                                            : "task"
                                    }
                                > <input
                                        type = 'text'
                                        className="changeNameTask" 
                                        style={{  display: 'none', width: "70%" }}
                                        placeholder="Entrez le nom de la tâche"
                                    />
                                    <span className="nomTache" style={{display : 'inline'}}>
                                        {task.task}
                                    </span>
                                </div>
                                <Button //bouton jaune de sauvegarde
                                    onClick={() => saveChange(task, this)}
                                    style={{ display: 'none' }}
                                    class="saveChange btn btn-warning"
                                >
                                    Sauvegarder
                                </Button>
                                <Button //bouton pour changer le nom
                                    onClick={() => changeName(task)}
                                    color="secondary"
                                    id="updateButton"
                                    class="btn btn-info"
                                >
                                    Changer nom
                                </Button>
                                <Button //bouton pour supprimer la tâche
                                    onClick={() => this.handleDelete(task._id)}
                                    color="secondary"
                                    id="deleteButton"
                                    class="btn btn-danger"
                                >
                                    Supprimer
                                </Button>
                            </Paper>
                        ))}
                    </div>
                </Paper>
            </div>
        );
    }
}

export default App;
