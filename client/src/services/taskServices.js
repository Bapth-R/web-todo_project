import axios from "axios";
const URL_BDD = "http://localhost:8080/api/tasks"; //url vers la BDD pour faire les requêtes

export function getTasks() { //utiliser pour récupérer la tâche dans la BDD
    return axios.get(URL_BDD);
}

export function addTask(task) { // utiliser pour ajouter la tâche à la base de donnée
    return axios.post(URL_BDD, task);
}

export function updateTask(id, task) { //permet de mettre à jour la tâche dans la BDD
    return axios.put(URL_BDD + "/" + id, task);
}

export function deleteTask(id) { // supprimer la tâche dans la BDD
    return axios.delete(URL_BDD + "/" + id);
}

